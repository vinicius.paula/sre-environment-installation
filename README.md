Este playbook realiza a instação de todos os itens descritos na documentação de OnBoarding, disponível em: https://zenvia.atlassian.net/wiki/spaces/SRECPAAS/pages/2434072644/Onboarding

Com exceção dos seguintes componentes:
- KAF
- Govc
- docker
- kubectl
- awscli

*Este playbook NÃO realiza importação de certificados, configuração de clusters e exportação de variavies.*

Para sua utilizaçação siga os passos abaixo: 

*Instalação do Ansible*

sudo apt update

sudo apt install software-properties-common

sudo add-apt-repository --yes --update ppa:ansible/ansible

sudo apt install ansible -y



*Adicione seu ip no arquivo hosts*


Mofique o conteudo do arquivo "ansible_cfg.txt" localizado em: /etc/ansible/ 

Utilizando as informações do arquivo: ansible_cfg.txt.tmp. 


Dentro da pasta que contem o playbook, utilize o comando abaixo para realizar as instalações:

ansible-playbook -i hosts playbook.yml

OBS: Caso aconteça o seguinte erro log file at /var/log/ansible.log is not writeable and we cannot create it, aborting. Utilize o comando com sudo.

sudo ansible-playbook -i hosts playbook.yml